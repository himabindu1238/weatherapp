import React, {useState} from "react";
import Search from "./Components/Search";
import WeatherComponent from "./Components/WeatherComponent";
import axios from "axios";

const API_KEY = "b934f85455286fdb92adbf2941ca43e5";
function App() {
    const [cityName, updateCity] = useState();
    const [weather, updateWeather] = useState();

    const fetchWeather = async (event) => {
        event.preventDefault();
        const response = await axios.get(`https://api.openweathermap.org/data/2.5/weather?q=${cityName}&appid=${API_KEY}`);
        updateWeather(response.data);
    };
  return (
      <div class="container">
          {weather ? ( <WeatherComponent weather={weather}/> ) : ( <Search updateCity={updateCity} fetchWeather={fetchWeather}/> )}
      </div>
  );
}

export default App;
