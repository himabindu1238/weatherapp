

const WeatherInfoComponent = (props) =>{
    return(
         <div class="infoContainer">
             <div class="infoIcon">
                 <img src={props.src} alt=""/>
             </div>
             <div class="infoLabel">
                 {props.value} <span>{props.name}</span>
             </div>
         </div>
    );
}

export default WeatherInfoComponent;