import React from "react";
import {Header, Image, Input, Segment} from "semantic-ui-react";

function Search(props){
    const {updateCity, fetchWeather} = props;
    return(
            <Segment placeholder>
                <Header icon>
                    <h1>Weather Forecast</h1>
                </Header>

                <Image className="" size='big'
                       src="https://www.kindpng.com/picc/m/209-2091974_partly-cloudy-clipart-image-with-rain-transparent-png.png"/>

                <Header icon>
                    <h3>Find the Weather for your City</h3>
                </Header>

                <form basic textAlign='center' onSubmit={fetchWeather} class="form">
                    <Input placeholder='City' onChange={(event) => updateCity(event.target.value)}/>
                    <button class="search-button" type={"submit"}>Search</button>
                </form>
            </Segment>

    );
}

export default Search;