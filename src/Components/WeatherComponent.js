import React from "react";
import WeatherInfoComponent from "./WeatherInfoComponent";
import { Header, Image, Segment} from "semantic-ui-react";
import Sunset from '../icon/sunset.png';
import Humidity from '../icon/humidity.png';
import Wind from '../icon/wind.png';
import Pressure from '../icon/pressure.png';

function WeatherComponent(props){
    const {weather} = props;
    const isDay = weather?.weather[0]?.icon?.includes("d");
    let logo = weather?.weather[0]?.icon;
    const getTime = (timestamp) => {
        return `${new Date(timestamp * 1000).getHours()} : ${new Date(timestamp*1000).getMinutes() }`;
    };
    const temp = Math.floor(weather?.main?.temp -273);
    let dayTiming = isDay ? "sunset" : "sunrise";
    return(
                <Segment placeholder>
                    <Header icon>
                        <h1>Weather Forecast</h1>
                    </Header>

                    <div class="weather-condition">
                        <span>{temp}°C | <span>{weather?.weather[0].description}</span></span>
                        <Image class="weather-logo" src={`http://openweathermap.org/img/wn/${logo}@2x.png`} size='small' floated='right' />
                    </div>

                    <Header icon>
                        <h1>{`${weather?.name}, ${weather?.sys?.country}`}</h1>
                    </Header>

                     <div class="WeatherInfoContainer">
                         <div class="weather-info-label">Weather Info</div>
                        <WeatherInfoComponent name={dayTiming} value={getTime(weather?.sys[dayTiming])} src={Sunset}/>
                        <WeatherInfoComponent name="Humidity" value={weather?.main?.humidity} src={Humidity}/>
                        <WeatherInfoComponent name="Wind" value={weather?.wind?.speed} src={Wind}/>
                         <WeatherInfoComponent name="Pressure" value={weather?.main?.pressure} src={Pressure}/>
                    </div>

                </Segment>
        );
}
export default WeatherComponent;